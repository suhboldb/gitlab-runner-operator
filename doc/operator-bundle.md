The information below will be used to populate the GitLab Runner Operator bundle and will be added to the Cluster Service Version upon review.

# GitLab Runner

GitLab Runner is the lightweight, highly-scalable agent that runs your build jobs and sends the results back to a GitLab instance. GitLab Runner works in conjunction with GitLab CI/CD, the open-source continuous integration service included with GitLab.

The GitLab Runner operator manages the lifecycle of GitLab Runner in Kubernetes or Openshift clusters. The operator aims to automate the tasks needed to run your CI/CD jobs in your container orchestration platform.

## Usage

 To link a GitLab Runner instance to a self-hosted GitLab instance or to [GitLab.com](https://gitlab.com), you first need to:

 1. Create a secret that contains the `runner-token`.

   `$ kubectl create secret generic runner-token-secret --from-literal runner-token="TOKEN_FROM_GITLAB_INSTANCE"`

 2. Create a GitLab Runner instance. Notice how the secret name is referenced in the example below.

    ```
    apiVersion: apps.gitlab.com/v1beta2
    kind: Runner
    metadata:
      name: example
    spec:
      gitlabUrl: https://gitlab.com
      token: runner-token-secret # Name of the secret that contains the Runner token
      tags: openshift, test
    ```


### Links

| Name | Link |
|------|------|
| GitLab Docs | https://docs.gitlab.com |
| GitLab Runner | https://docs.gitlab.com/runner/ |
| GitLab CI/CD | https://docs.gitlab.com/ee/ci/quick_start/ |
| Gitlab Runner Operator | https://gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator |


### Maintainers

| Name | Email |
|------|-------|
| Developer 1 | developer@gitlab.com |

### Categories
- Integration & Delivery
- Developer Tools

### Provider

GitLab Inc.
