package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"

	"github.com/flynn/go-shlex"
)

// CommandBuilder is a builder for a Command.
type CommandBuilder struct {
	env        map[string]string
	dir        string
	out        io.Writer
	err        io.Writer
	in         io.Reader
	ignoreErrs []string
}

// NewCommand creates a new CommandBuilder.
func NewCommand() CommandBuilder {
	return CommandBuilder{
		env: map[string]string{},
	}
}

// Dir sets the working directory for the command.
func (c CommandBuilder) Dir(dir string) CommandBuilder {
	c.dir = dir
	return c
}

// Env sets an environment variable for the command.
func (c CommandBuilder) Env(key, value string) CommandBuilder {
	c.env[key] = value
	return c
}

// Envs sets the environment variables for the command.
func (c CommandBuilder) Envs(envs map[string]string) CommandBuilder {
	for k, v := range envs {
		c.Env(k, v)
	}
	return c
}

// IgnoreErrs sets the errors to ignore.
func (c CommandBuilder) IgnoreErrs(errs ...string) CommandBuilder {
	c.ignoreErrs = errs
	return c
}

// Out sets the output writer for the command.
func (c CommandBuilder) Out(out io.Writer) CommandBuilder {
	c.out = out
	return c
}

// Err sets the error writer for the command.
func (c CommandBuilder) Err(err io.Writer) CommandBuilder {
	c.err = err
	return c
}

// In sets the input reader for the command.
func (c CommandBuilder) In(in io.Reader) CommandBuilder {
	c.in = in
	return c
}

// Command is a wrapper around exec.Cmd.
type Command struct {
	*exec.Cmd

	ignoreErrs []string
	out        *bytes.Buffer
}

// Cmd sets the command to execute.
func (c CommandBuilder) Cmd(cmd string, args ...any) *Command {
	// TODO handle err:
	cmds, _ := shlex.Split(fmt.Sprintf(cmd, args...))
	return newCommand(c, cmds)
}

// CmdArgs sets the arguments for the command.
func (c CommandBuilder) CmdArgs(args ...string) *Command {
	return newCommand(c, args)
}

func newCommand(builder CommandBuilder, cmds []string) *Command {
	// TODO context:
	cmd := exec.Command(cmds[0], cmds[1:]...)
	cmd.Dir = builder.dir

	if builder.out == nil {
		builder.out = os.Stdout
	}
	cmd.Stdout = builder.out

	if builder.err == nil {
		builder.err = os.Stderr
	}
	cmd.Stderr = builder.err

	cmd.Stdin = builder.in

	for k, v := range builder.env {
		cmd.Env = append(cmd.Env, fmt.Sprintf("%s=%s", k, v))
	}

	var out *bytes.Buffer
	if len(builder.ignoreErrs) > 0 {
		out = &bytes.Buffer{}
		var stderr io.Writer = out
		if cmd.Stderr != nil {
			stderr = io.MultiWriter(cmd.Stderr, out)
		}

		cmd.Stderr = stderr
	}

	return &Command{
		ignoreErrs: builder.ignoreErrs,
		out:        out,
		Cmd:        cmd,
	}
}

// Run runs the command.
func (c *Command) Run() error {
	fmt.Println(fmt.Sprintf("Running command %s in %s", c.Cmd.Args, c.Cmd.Dir))
	err := c.Cmd.Run()
	if err != nil && len(c.ignoreErrs) > 0 {
		outString := c.out.String()
		for _, ignoreErr := range c.ignoreErrs {
			// TODO: we could make these errors regexes
			if strings.Contains(err.Error(), ignoreErr) || strings.Contains(outString, ignoreErr) {
				fmt.Println(fmt.Sprintf("Ignoring error %s", strings.TrimSpace(err.Error())))
				return nil
			}
		}
	}

	return err
}
