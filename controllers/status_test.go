package controllers

import (
	"testing"

	"gotest.tools/assert"
)

func TestGetRegistrationStatus(t *testing.T) {
	tests := map[string]struct {
		log    string
		err    error
		status string
	}{
		"registration status unavailable": {
			log: `Runner registered successful`,
			err: errRegistrationStatusUnavailable,
		},
		"registration succeeded": {
			log:    `Registering runner... succeeded                     runner=XXXX`,
			status: "succeeded",
		},
		"registration forbidded": {
			log:    `Registering runner... forbidden (check registration token)                     runner=XXXX`,
			status: "forbidden",
		},
		"registration client error": {
			log:    `Registering runner... client error                     runner=XXXX`,
			status: "client error",
		},
		"registration failed": {
			log:    `Registering runner... failed                     runner=XXXX`,
			status: "failed",
		},
		"authentication token verification is valid": {
			log:    `Verifying runner... is valid                     runner=XXXX`,
			status: "is valid",
		},
		"authentication token verification is alive": {
			log:    `Verifying runner... is alive                     runner=XXXX`,
			status: "is alive",
		},
		"authentication token verification is not valid": {
			log:    `Verifying runner... is not valid                     runner=XXXX`,
			status: "is not valid",
		},
		"authentication token verification is removed": {
			log:    `Verifying runner... is removed                     runner=XXXX`,
			status: "is removed",
		},
		"authentication token verification client error": {
			log:    `Verifying runner... client error                     runner=XXXX`,
			status: "client error",
		},
		"authentication token verification failed": {
			log:    `Verifying runner... failed                     runner=XXXX`,
			status: "failed",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			status, err := getRegistrationStatus(test.log)
			if test.err != nil {
				assert.Error(t, err, errRegistrationStatusUnavailable.Error())
				return
			}

			assert.Equal(t, status, test.status)
		})
	}
}
